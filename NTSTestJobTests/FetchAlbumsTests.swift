//
//  FetchAlbumsTests.swift
//  NTSTestJobTests
//
//  Created by Gaurav Sokhal on 1/31/19.
//  Copyright © 2019 Veeral Bhateja. All rights reserved.
//

import XCTest
@testable import NTSTestJob

class FetchAlbumsTests: XCTestCase {
    
    var viewModel: AlbumsViewModel?
    
    override func setUp() {
        super.setUp()
        //Initialising Object
        viewModel = AlbumsViewModel()
    }

    override func tearDown() {
        super.tearDown()
        
        viewModel = nil
    }

    func testFetchAlbums() {
        //Cheking before calling API
        XCTAssertEqual(viewModel?.numberOfSections(), 0)
        
        //Set expectations
        let promise = expectation(description: "Count Updated")
        
<<<<<<< HEAD
        viewModel?.fetchData(url: "http://jsonplaceholder.typicode.com/photos") { (success, err) in
=======
        viewModel?.fetchData(url: "http://jsonplaceholder.typicode.com/photos") { (success) in
            
>>>>>>> 3a2f535... -Added new view model: AlbumsViewModel
            if success == true {
                promise.fulfill()
            }
        }
        //Wait for api results to come
        waitForExpectations(timeout: 5, handler: nil)
        
        //Sections
        XCTAssertNotEqual(viewModel?.numberOfSections(), 0)
        //Items at section 0
        XCTAssertNotEqual(viewModel?.numberOfItemsInSection(section: 0), 0)
        //Album not nil at index 0
        XCTAssertNotNil(viewModel?.albumb(at: IndexPath(row: 0, section: 0)))
    }

    
    func testFetchAlbumsFails() {
        //Cheking before calling API
        XCTAssertEqual(viewModel?.numberOfSections(), 0)
        
        //Set expectations
        let promise = expectation(description: "Count Not Updated")

<<<<<<< HEAD
        viewModel?.fetchData(url: "jsonplaceholder.typicode.com/") { (success, err) in
=======
        viewModel?.fetchData(url: "jsonplaceholder.typicode.com/") { (success) in
>>>>>>> 3a2f535... -Added new view model: AlbumsViewModel
            
            if success == false {
                promise.fulfill()
            }
        }
<<<<<<< HEAD
        
=======
>>>>>>> 3a2f535... -Added new view model: AlbumsViewModel
        //Wait for api results to come
        waitForExpectations(timeout: 5, handler: nil)
        
        //Sections
        XCTAssertEqual(viewModel?.numberOfSections(), 0)
        //Items at section 0
        XCTAssertEqual(viewModel?.numberOfItemsInSection(section: 0), 0)
    }
    
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
