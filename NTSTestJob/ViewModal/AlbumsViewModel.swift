//
//  AlbumsViewModel.swift
//  NTSTestJob
//
//  Created by Veeral Bhateja on 1/31/19.
//  Copyright © 2019 Veeral Bhateja. All rights reserved.
//

import UIKit

<<<<<<< HEAD

class AlbumsViewModel  {

=======
class AlbumsViewModel  {

    //MARK: Constants
    
>>>>>>> 3a2f535... -Added new view model: AlbumsViewModel
    //MARK: Variables
    fileprivate var modalAlbums = [AlbumViewModel]()
    fileprivate var refreshController: RefreshController? = nil
    fileprivate var albumIdArray: [Int]?
}

//MARK: CollectionView Methods
extension AlbumsViewModel {
    
    /*
     This method gets the album at provided indexPath and return it.
     Params:
     -indexPath: Indexpath of album
     Returns:
     -It returns album as AlbumViewModel
     */
    func albumb(at indexPath: IndexPath) -> AlbumViewModel {
        let albumViewModel = modalAlbums.filter({$0.albumId == albumIdArray?[indexPath.section]})
<<<<<<< HEAD
=======
        
>>>>>>> 3a2f535... -Added new view model: AlbumsViewModel
        return albumViewModel[indexPath.item]
    }
    
    /*
     This method returns number of items in section.
     Params:
     -section: At which section count of items are required.
     Returns:
     -It returns number of items in section as Int
     */
    func numberOfItemsInSection(section: Int) -> Int {
        let elementsInSection = modalAlbums.filter({$0.albumId == albumIdArray?[section]})
        return elementsInSection.count
    }
    
    /*
     This method returns title for header at indexPath.
     Params:
     -indexPath: At which indexPath title is required.
     Returns:
     -It returns title of header as Int
     */
<<<<<<< HEAD
    func headerTitleFor(indexPath: IndexPath) -> String {
        return (Array(NSOrderedSet(array: self.modalAlbums.map({ $0.albumName ?? ""}))) as! [String])[indexPath.section]
//        return albumIdArray?[indexPath.section] ?? 0
=======
    func headerTitleFor(indexPath: IndexPath) -> Int {
        return albumIdArray?[indexPath.section] ?? 0
>>>>>>> 3a2f535... -Added new view model: AlbumsViewModel
    }
    
    /*
     This method returns number of sections.
     Returns:
     -It returns number of sections as Int
     */
    func numberOfSections() -> Int {
        return albumIdArray?.count ?? 0
    }
    
    /*
     This method calculates the size of item and returns.
     Params:
     -indexPath: At indexpath the size needs to be calculated
     -collectionView: Object of collectionview for which we are calculating the size
     Returns:
     -It returns size of item as CGSize
     */
    func sizeForItemAt(indexPath: IndexPath, collectionView: UICollectionView) -> CGSize {
        let padding: CGFloat =  15
        let collectionViewSize = (collectionView.frame.size.width / 2 ) - padding
        return CGSize(width: collectionViewSize, height: collectionViewSize)
    }
    
    /*
     This method returns inset for section.
     Params:
     -section: At section the inset required.
     Returns:
     -It returns edge inset for section as UIEdgeInsets
     */
    func insetForSectionAt(section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10,left: 10,bottom: 10,right: 10)
    }
    
    /*
     This method returns minimum interitem spacing for given section.
     Params:
     -section: At section the inset required.
     Returns:
     -It returns minimum interitem spacing for section as CGFloat
     */
    func minimumInteritemSpacingForSectionAt(section: Int) -> CGFloat {
        return 0
    }
    
}

//MARK:- API Call
extension AlbumsViewModel {
    /*
     This method fetches the albums from url, parses the json and populates the data into variables.
     Param:
     -url: Url of albums to be fetched of type String
     -completion: completion handler with bool parameter telling if its success or not.
     */
<<<<<<< HEAD
    func fetchData(url: String, completion: @escaping (_ success: Bool, _ err: Error?) -> ()) {
        DispatchQueue.global().async {
            Service.shared.fetchAlbums(url: url, modal: [ModalAlbums].self) { (albums, err) in
                if let err = err {
                    completion(false, err)
=======
    func fetchData(url: String, completion: @escaping (_ success: Bool) -> ()) {
        DispatchQueue.global().async {
            Service.shared.fetchAlbums(url: url, modal: [ModalAlbums].self) { (albums, err) in
                if let err = err {
                    print("Failed to fetch albums:", err)
                    completion(false)
>>>>>>> 3a2f535... -Added new view model: AlbumsViewModel
                    return
                }
                //Filter and populates the variables
                self.modalAlbums = albums?.map({return AlbumViewModel(album: $0)}) ?? []
                self.albumIdArray = Array(NSOrderedSet(array: self.modalAlbums.map({ $0.albumId ?? 0}))) as? [Int]
<<<<<<< HEAD
                completion(true, err)
=======
                completion(true)
>>>>>>> 3a2f535... -Added new view model: AlbumsViewModel
            }
        }
    }
}
