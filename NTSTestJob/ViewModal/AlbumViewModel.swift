//
//  CourseViewModel.swift
//  mvc_to_mvvm_demo
//
//  Created by Brian Voong on 7/3/18.
//  Copyright © 2018 Brian Voong. All rights reserved.
//

import Foundation
import UIKit

struct AlbumViewModel {
    
    //MARK:- Constants
    let id:Int?
    let url:String?
    let thumbnailUrl:String?
    let albumId: Int?
    let title: String?
    let albumName: String?

    
    // Dependency Injection (DI)
    init(album: ModalAlbums) {
        self.id = album.id
        self.url = album.url
        self.thumbnailUrl = album.thumbnailUrl
        self.albumId = album.albumId
        self.albumName = "Album Name :- " + String(album.albumId ?? 0)
        self.title = album.title
    }
}
