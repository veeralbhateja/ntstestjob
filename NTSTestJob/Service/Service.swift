//
//  Service.swift
//  MVC
//
//  Created by Brian Voong on 6/30/18.
//  Copyright © 2018 Brian Voong. All rights reserved.
//

import Foundation

class Service: NSObject {
    static let shared = Service()
    
    func fetchAlbums<T:Decodable>(url: String, modal: T.Type,completion: @escaping (T?, Error?) -> ()) {
        let urlString = url
        guard let url = URL(string: urlString) else { return }
        
        //Show Loader
        Loader.showLoader()
        
        URLSession.shared.dataTask(with: url) { (data, resp, err) in
            
            //Hide Loader
            Loader.hideLoader()
            
            if let err = err {
                completion(nil, err)
//                print("Failed to fetch courses:", err)
                return
            }
            
            // check response
            guard let data = data else { return }
            do {
                let courses = try JSONDecoder().decode(modal, from: data)
                DispatchQueue.main.async {
                    completion(courses, nil)
                }
            } catch let jsonErr {
                print("Failed to decode:", jsonErr)
            }
            }.resume()
    }
}
