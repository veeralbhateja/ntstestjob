//
//  Ext + UIViewController.swift
//  NTSTestJob
//
//  Created by Veeral Bhateja on 30/01/19.
//  Copyright © 2019 Veeral Bhateja. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController{
    
    // MARK:- string type enum
    enum Identifier: String{
        case detailsVC = "DetailsVC"
        static let value = [detailsVC]
    }
    
    /*
     This method instantiate ViewController with its identifier and returns ViewController
     Params:-
        identifier - Name of the ViewController's Storyboard ID
     */    
    func navigateToVC(identifier: Identifier) -> UIViewController{
        return (self.storyboard?.instantiateViewController(withIdentifier: identifier.rawValue))!
    }
    
}

