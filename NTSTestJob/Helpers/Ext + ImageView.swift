//
//  Ext + Kingfisher.swift
//
//  This is UIImageView extension which helps in setting image to it.
//
//  NTSTestJob
//
//  Created by Veeral Bhateja on 29/01/19.
//  Copyright © 2019 Veeral Bhateja. All rights reserved.
//

import UIKit
import Kingfisher

extension UIImageView {
    
<<<<<<< HEAD
    // MARK:- string type enum
=======
>>>>>>> 3a2f535... -Added new view model: AlbumsViewModel
    enum AssetIdentifier: String {
        case placeholder = "placeholder"
        static let value = [placeholder]
    }
    
    /*
     This method downloads the image from given url and sets it to imageview.
     Params:
     - imageUrl: This is string type parameter, which accepts the url from which the image has to be downloaded.
     - placeholder: This is an enum AssetIdentifier type, which tells that which placeholder image to be shown if it fails to download the image from given url.
     */
    func setImage(imageURL: String, placeholder: AssetIdentifier) {
        var kf = self.kf
        kf.indicatorType = .activity
        kf.setImage(with: URL(string: imageURL), placeholder: UIImage(named: placeholder.rawValue), options: nil, progressBlock: nil, completionHandler: { (image, error, cacheType, imageURL) in
            if let error = error {
//                print("Error: \(error)")
            } else if let image = image {
                self.image = image
//                print("Image: \(image). Got from: \(cacheType)")
            }
        })
    }
    
}
