//
//  Ext + UIImage.swift
//  NTSTestJob
//
//  Created by Veeral Bhateja on 30/01/19.
//  Copyright © 2019 Veeral Bhateja. All rights reserved.
//

import Foundation
import UIKit

extension UIImage{
    // MARK:- string type enum
    enum AssetIdentifier: String {
        case placeholder = "placeholder"
        static let value = [placeholder]
    }
    
    // MARK:- set Image with name
    convenience init(assetIdentifier: AssetIdentifier){
        self.init(named: assetIdentifier.rawValue)!
    }
}
