//
//  RefreshController.swift
//  Merchant
//
//  Created by Veeral Bhateja on 07/09/18.
//  Copyright © 2018 Veeral Bhateja. All rights reserved.
//

import UIKit

@objc protocol RefreshControllerDelegate: class {
    func didStarted(_ refreshControl: UIRefreshControl)
    @objc optional func didStopped(_ refreshControl: UIRefreshControl)
}

public class RefreshController{
    
    fileprivate var refreshControl = UIRefreshControl()
    fileprivate weak var delegateRefresh: RefreshControllerDelegate? = nil
    fileprivate var colleciton: UICollectionView!
    fileprivate let kpullRefresh = "Pull To Refresh"
    
    //    MARK:- initializing RefreshController
    init(delegate: RefreshControllerDelegate, collectionView: UICollectionView){
        self.delegateRefresh = delegate
        self.colleciton = collectionView
        startRefreshing()
    }
    
    // MARK:- Start Refreshing
    fileprivate func startRefreshing(){
        refreshControl.attributedTitle = NSAttributedString(string: kpullRefresh)
        refreshControl.addTarget(self, action: #selector(refresh(_:)), for: UIControl.Event.valueChanged)
        colleciton.addSubview(refreshControl)
    }
    
    @objc fileprivate func refresh(_ sender: UIRefreshControl){
      delegateRefresh?.didStarted(sender)
    }
    
//    MARK:- Stop Refreshing after loading is done and remove it from view
    func stopRefreshing(){
        colleciton.willRemoveSubview(refreshControl)
        refreshControl.endRefreshing()
    }

    // MARK:- DeInitialise refreshController
    deinit {
        refreshControl.removeFromSuperview()
    }
}
