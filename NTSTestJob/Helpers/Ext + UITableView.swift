//
//  Ext + UITableView.swift
//  NTSTestJob
//
//  Created by Veeral Bhateja on 30/01/19.
//  Copyright © 2019 Veeral Bhateja. All rights reserved.
//

import Foundation
import UIKit

extension UITableView{
    
    // string type enum
    enum NibName: String{
        // Names of nib for tableview
        case detailTableCell = "DetailTableCell"
        static let value = [detailTableCell]
    }
    
    // This method gurantees to Register nib for UITableView while selecting a nibName.
    func registerNib(nibName: NibName){
        self.register(UINib(nibName: nibName.rawValue, bundle: nil), forCellReuseIdentifier: nibName.rawValue)
    }
    
    
    /*
     Used by the delegate to acquire an already allocated cell by allocating a new one.
     Reuseable Identifier. Guarantees cell is returned and resized properly
     */
    
    func dequeueReusableCell(identifier: NibName, indexPath: IndexPath) -> UITableViewCell{
        return self.dequeueReusableCell(withIdentifier: identifier.rawValue, for: indexPath)
    }
    
}
