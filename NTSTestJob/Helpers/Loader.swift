//
//  Loader.swift
//  NTSTestJob
//
//  Created by Veeral Bhateja on 1/31/19.
//  Copyright © 2019 Veeral Bhateja. All rights reserved.
//

import Foundation
import MBProgressHUD

<<<<<<< HEAD

=======
>>>>>>> 3a2f535... -Added new view model: AlbumsViewModel
class Loader: NSObject {
    
    /*
     This method shows loader in view
     Params:-
<<<<<<< HEAD
     -animated: Animation to be performed while showing loader as Bool. Default value is true.
=======
     -animated: Animation to be performed while showing loader as Bool. Default vallue is true.
>>>>>>> 3a2f535... -Added new view model: AlbumsViewModel
     */
    class func showLoader(animated: Bool = true) {
        DispatchQueue.main.async {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let showInView = appDelegate.window?.rootViewController?.view
            MBProgressHUD.showAdded(to: showInView ?? UIView(), animated: animated)
        }
    }
    
    /*
     This method hides loader from view
     Params:-
<<<<<<< HEAD
     -animated: Animation to be performed while hiding loader as Bool. Default value is true.
=======
     -animated: Animation to be performed while hiding loader as Bool. Default vallue is true.
>>>>>>> 3a2f535... -Added new view model: AlbumsViewModel
     */
    class func hideLoader(animated: Bool = true) {
        DispatchQueue.main.async {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let showInView = appDelegate.window?.rootViewController?.view
            MBProgressHUD.hide(for: showInView ?? UIView(), animated: animated)
        }
    }
    
}
