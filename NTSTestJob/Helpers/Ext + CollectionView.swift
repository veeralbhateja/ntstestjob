//
//  Ext + CollectionView.swift
//  NTSTestJob
//
//  Created by Veeral Bhateja on 29/01/19.
//  Copyright © 2019 Veeral Bhateja. All rights reserved.
//

import UIKit

extension UICollectionView{
    
    // MARK:- string type enum
    enum NibName: String{
        // Names of nib for tableview
        case imageCollectionCell = "ImageCollectionCell"
        case imageCollectionHeaderView = "ImageCollectionHeaderView"
        static let value = [imageCollectionCell,imageCollectionHeaderView]
    }
    
    // MARK:-  Register nib for CollectionView
    /*
     For each reuse identifier that the collection view will use, register either a class or a nib from which to instantiate a cell.
     */
    func registerNib(nibName: NibName){
        self.register(UINib(nibName: nibName.rawValue, bundle: nil), forCellWithReuseIdentifier: nibName.rawValue)
    }
    
    // MARK:- Reuseable Identifier for Header Footer View.
    func dequeueReusableSupplementaryView(kind: String, identifier: NibName, indexPath: IndexPath) -> UICollectionReusableView{
        return self.dequeueReusableSupplementaryView(
            ofKind: kind, withReuseIdentifier: identifier.rawValue,for: indexPath)
    }
    
    /*
     This method dequeue ReusableCell for 
     Params:-
     identifier - Name of the ViewController's Storyboard ID
     */
    
    // MARK:- Reuseable Identifier. Guarantees a cell is returned and resized properly
    func dequeueReusableCell(identifier: NibName, indexPath: IndexPath) -> UICollectionViewCell{
        return self.dequeueReusableCell(withReuseIdentifier: identifier.rawValue, for: indexPath)
    }
    
}
