//
//  BaseClass.swift
//
//  Created by Veeral Bhateja on 29/01/19
//  Copyright (c) . All rights reserved.
//

import Foundation

// This Modal is to decode the respose from JSON.

struct ModalAlbums: Decodable {
    
    // MARK:- Variables
    public var id: Int?
    public var url: String?
    public var thumbnailUrl: String?
    public var albumId: Int?
    public var title: String?
}
