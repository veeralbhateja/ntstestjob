//
//  ImageCollectionHeaderView.swift
//  NTSTestJob
//
//  Created by Veeral Bhateja on 31/01/19.
//  Copyright © 2019 Veeral Bhateja. All rights reserved.
//

import UIKit

class ImageCollectionHeaderView: UICollectionReusableView {
    //MARK:- Outlets
    @IBOutlet weak var labelHeader: UILabel!
    
    // MARK:- Variable
    var headerTitle: String! {
        // Setting Label for Header
        didSet {
            labelHeader.text = headerTitle
        }
    }
    
}
