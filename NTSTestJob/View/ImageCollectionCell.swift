//
//  imageCollectionCell.swift
//  NTSTestJob
//
//  Created by Veeral Bhateja on 29/01/19.
//  Copyright © 2019 Veeral Bhateja. All rights reserved.
//

import UIKit

class ImageCollectionCell: UICollectionViewCell {
    
    //MARK:- Outlets
    @IBOutlet weak var imageViewAlbum: UIImageView!
    @IBOutlet weak var viewBG: UIView!
    
    //MARK:- Variable
    var albumViewModel: AlbumViewModel! {
        // Setting Image
        didSet {
            imageViewAlbum.setImage(imageURL: albumViewModel.thumbnailUrl ?? "", placeholder: .placeholder)
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
