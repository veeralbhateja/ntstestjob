//
//  DetailTableCell.swift
//  NTSTestJob
//
//  Created by Veeral Bhateja on 30/01/19.
//  Copyright © 2019 Veeral Bhateja. All rights reserved.
//

import UIKit

class DetailTableCell: UITableViewCell {
    
    //MARK:- Outlets
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var imageViewMain: UIImageView!
    
    //MARK:- Variable
    var albumViewModel: AlbumViewModel! {
        didSet {
            // Setting Image to ImageView using KingFisher Pod
            imageViewMain.setImage(imageURL: albumViewModel.url ?? "", placeholder: .placeholder)
            // Setting Title to label
            labelTitle.text = albumViewModel.title
        }
    }


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
