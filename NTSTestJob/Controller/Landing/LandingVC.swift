//
//  LandingVC.swift
//  NTSTestJob
//
//  Created by Veeral Bhateja on 29/01/19.
//  Copyright © 2019 Veeral Bhateja. All rights reserved.
//

import UIKit

class LandingVC: UIViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var collectionImageView: UICollectionView!
    
    //MARK:- Constants
    //This url you can change while reusing it
    let url = "http://jsonplaceholder.typicode.com/photos"
    
    //MARK:- Variables
    fileprivate var viewModel: AlbumsViewModel = AlbumsViewModel()
    fileprivate var refreshController: RefreshController? = nil
    fileprivate var albumIdArray: [Int]?
    
    
    //MARK:- View's LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()

        //Do initial setup
        self.doInitialSetup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //Initialise RefreshController
        refreshController = RefreshController(delegate: self, collectionView: collectionImageView)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        refreshController = nil
    }
}

//MARK: Helper Methods
extension LandingVC {
    
    func doInitialSetup() {
        //Set title
        self.title = "Albums"
        
        //Register Nib
        registerNib()
        
        //Fetch albums
        self.fetchData()
    }
    
    //Register Nib
    func registerNib(){
        collectionImageView.registerNib(nibName: .imageCollectionCell)
    }
    
    //Fetch albums
    fileprivate func fetchData() {
        self.viewModel.fetchData(url: url) { (success, err) in
            self.refreshController?.stopRefreshing()

            if let error = err{
                self.showAlert(error.localizedDescription)
            }
            if success == true {
                //End refreshing
                self.refreshController?.stopRefreshing()
                //Reload data in collectionview
                self.collectionImageView.reloadData()
            }
        }
    }
    
    // Alert PopUp
    fileprivate func showAlert(_ message: String) {
        // create the alert
        let alert = UIAlertController(title: "Alert", message: message, preferredStyle: UIAlertController.Style.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        
        alert.addAction(UIAlertAction(title: "Retry", style: UIAlertAction.Style.default, handler: { action in
            self.fetchData()
        }))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    
}

//MARK: UICollectionView delegate and datasource methods
extension LandingVC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(identifier: .imageCollectionCell, indexPath: indexPath) as! ImageCollectionCell
        cell.albumViewModel = viewModel.albumb(at: indexPath)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return viewModel.numberOfItemsInSection(section: section)
    }
    
    func collectionView(_ collectionView: UICollectionView,viewForSupplementaryElementOfKind kind: String,at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
        case UICollectionView.elementKindSectionHeader:
            guard
                let headerView = collectionView.dequeueReusableSupplementaryView(kind: kind, identifier: .imageCollectionHeaderView, indexPath: indexPath) as? ImageCollectionHeaderView
                else {
                    fatalError("Invalid view type")
            }
            headerView.headerTitle = viewModel.headerTitleFor(indexPath: indexPath)
            return headerView
        default:
            assert(false, "Invalid element type")
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return viewModel.numberOfSections()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //Open Detail viewcontroller
        let detailVC = self.navigateToVC(identifier: .detailsVC) as! DetailsVC
        detailVC.rAlbum = viewModel.albumb(at: indexPath)
        self.navigationController?.pushViewController(detailVC, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return viewModel.sizeForItemAt(indexPath: indexPath, collectionView: collectionView)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return viewModel.insetForSectionAt(section: section)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return viewModel.minimumInteritemSpacingForSectionAt(section: section)
    }
    
}

//MARK: RefreshControllerDelegate Methods
extension LandingVC: RefreshControllerDelegate{
    
    func didStarted(_ refreshControl: UIRefreshControl) {
        //Fetch data
        self.fetchData()
    }
}
