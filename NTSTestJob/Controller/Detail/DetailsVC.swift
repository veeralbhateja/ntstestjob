//
//  DetailsVC.swift
//  NTSTestJob
//
//  Created by Veeral Bhateja on 30/01/19.
//  Copyright © 2019 Veeral Bhateja. All rights reserved.
//

import UIKit


class DetailsVC: UIViewController {
    
    //MARK:- Outlet
    @IBOutlet weak var tableViewDetail: UITableView!

    //MARK:- Variable
    var rAlbum: AlbumViewModel?
    
    //MARK:- View's LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
<<<<<<< HEAD:NTSTestJob/Controller/Detail/DetailsVC.swift
        //Do initial setup
        doInitialSetup()
    }
}

//MARK: Helper Methods
extension DetailsVC{
    func doInitialSetup() {
        //Set title
        self.title = "Detail"

        //Register Nib
=======
        self.title = "Detail"
>>>>>>> 3a2f535... -Added new view model: AlbumsViewModel:NTSTestJob/Controller/DetailsVC.swift
        registerNib()

        //Setting TableViewDynamic height and Reload
        tableViewDetail.estimatedRowHeight = 100
        tableViewDetail.rowHeight = UITableView.automaticDimension
        tableViewDetail.reloadData()
    }
    
    func registerNib(){
        // Register UITableView Nib by selecting nibName from list of enums.
        tableViewDetail.registerNib(nibName: .detailTableCell)
    }
    

}

//MARK: UITableView delegate and datasource methods
extension DetailsVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(identifier: .detailTableCell, indexPath: indexPath) as! DetailTableCell
        // Setting Value to cell
        cell.albumViewModel = rAlbum
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
